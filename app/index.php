<?php
require_once 'google-api-php-client-2.2.3/vendor/autoload.php';

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Classroom API PHP Quickstart');
    $client->setScopes(Google_Service_Classroom::CLASSROOM_COURSES);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}


// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Classroom($client);

// Print the first 10 courses the user has access to.
$optParams = array(
  'pageSize' => 10
);
$results = $service->courses->listCourses($optParams);

if (count($results->getCourses()) == 0) {
  print "No courses found.\n";
} else {
  print "Courses:\n";
  foreach ($results->getCourses() as $course) {
    printf("%s (%s)\n", $course->getName(), $course->getId());
  }
}
$course = new Google_Service_Classroom_Course(array(
    'name' => '10th Grade Biology',
    'section' => 'Period 2',
    'descriptionHeading' => 'Welcome to 10th Grade Biology',
    'description' => 'We\'ll be learning about about the structure of living ' .
                     'creatures from a combination of textbooks, guest ' .
                     'lectures, and lab work. Expect to be excited!',
    'room' => '301',
    'ownerId' => 'me',
    'courseState' => 'PROVISIONED'
  ));
  $course = $service->courses->create($course);
  printf("Course created: %s (%s)\n", $course->name, $course->id);